library ieee;
use ieee.std_logic_1164.all;

entity tb_MIPS is
end tb_MIPS;

architecture behavior of tb_MIPS is
	-- Component Declaration for the single-cycle MIPS Processor in VHDL
	component MIPS
		port
		(
			clk        : in std_logic;
			reset      : in std_logic			
		);
	end component;
	
	--Inputs
	signal clk          : std_logic := '0';
	signal reset        : std_logic := '0';
	--Outputs
	signal pc_out       : std_logic_vector(15 downto 0);
	signal alu_result   : std_logic_vector(15 downto 0);
	-- Clock period definitions
	constant clk_period : time := 10 ns;

begin
	-- Instantiate the for the single-cycle MIPS Processor in VHDL
	test : entity work.MIPS(Behavioral)
	port map (
		clk        => clk,
		reset      => reset,		
	);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	
	-- Stimulus process
	stim_proc : process
	begin
		reset <= '1';
		wait for clk_period * 10;
		reset <= '0';
		
		wait;
	end process;

end;
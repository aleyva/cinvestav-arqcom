LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
  
ENTITY tb_MIPS IS
END tb_MIPS;
 
ARCHITECTURE behavior OF tb_MIPS IS 

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
	
	--outputs
	signal pc_out	   : std_logic_vector(15 downto 0);
	signal alu_result : std_logic_vector(15 downto 0);


   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
	test: entity work.MIPS(Behavioral)   
		PORT MAP (
          clk => clk,
          rst => reset,
			 pc_out => pc_out,
			 alu_result => alu_result 
        );


   -- Clock process definitions
   clk_process :process
   begin		
      clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;		
		
	-- Stimulus process
	stim_proc: process
	begin  
		reset <= '1';
		wait for clk_period*10;
		reset <= '0';
      -- insert stimulus here 
      wait;
   
   end process;

END;












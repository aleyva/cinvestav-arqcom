library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use STD.textio.all; -- Required for freading a file

entity instruction_memory is
	port
	(
		pc          : in std_logic_vector(15 downto 0);
		instruction : out std_logic_vector(15 downto 0)
	);
end instruction_memory;

architecture Behavioral of instruction_memory is
	signal rom_addr : std_logic_vector(3 downto 0);
	type ROM_type is array (0 to 15) of std_logic_vector(15 downto 0);
   signal rom : ROM_type := ((others => (others => '0')));
   --signal rom : ROM_type := (
	--	constant rom : ROM_type := (
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000",
	-- 	x"0000"
	-- );
begin

	-- The process for reading the instructions into memory
    process 
        file file_pointer : text;
        variable line_content : string(1 to 16);
        variable line_num : line;
        variable i: integer := 0;
        variable j : integer := 0;
        variable char : character:='0'; 
    
        begin
        -- Open instructions.txt and only read from it
        file_open(file_pointer, "instructions.txt", READ_MODE);
        -- Read until the end of the file is reached  
        while not endfile(file_pointer) loop
            readline(file_pointer,line_num); -- Read a line from the file
            READ(line_num,line_content); -- Turn the string into a line (looks wierd right? Thanks Obama)
            -- Convert each character in the string to a bit and save into memory
            for j in 1 to 16 loop        
                char := line_content(j);
                if(char = '0') then
                    rom(i)(16-j) <= '0';
                else
                    rom(i)(16-j) <= '1';
                end if; 
            end loop;
            i := i + 1;
        end loop;
--        if i > 0 then
--            last_instr_address <= std_logic_vector(to_unsigned((i-1)*2, last_instr_address'length));
--        else
--            last_instr_address <= x"0000";
--        end if;

        file_close(file_pointer); -- Close the file 
        wait;
    end process;

	rom_addr    <= pc(4 downto 1);
	instruction <= rom(to_integer(unsigned(rom_addr))) when pc < x"0020" else x"0000";

end Behavioral;
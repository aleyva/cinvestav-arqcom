library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity alu_control_comp is
	port
	(
		actl_alu_control 	: out std_logic_vector(2 downto 0);
		actl_aluop       	: in std_logic_vector(1 downto 0);
		actl_alu_instruction : in std_logic_vector(2 downto 0)
	);
end alu_control_comp;

architecture Behavioral of alu_control_comp is
begin
	process (actl_aluop, actl_alu_instruction)
	begin
		case actl_aluop is
			when "00" =>
				actl_alu_control <= actl_alu_instruction(2 downto 0);
			when "01" =>
				actl_alu_control <= "001";
			when "10" =>
				actl_alu_control <= "100";
			when "11" =>
				actl_alu_control <= "000";
			when others => 
				actl_alu_control <= "000";
		end case;
	end process;
end Behavioral;
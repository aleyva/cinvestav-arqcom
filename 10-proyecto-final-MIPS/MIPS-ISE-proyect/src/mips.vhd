library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_signed.all;

entity MIPS is
	port
	(
		clk		  : in std_logic;
		rst 	     : in std_logic;
		pc_out	  : out std_logic_vector(15 downto 0);
		alu_result : out std_logic_vector(15 downto 0)
	);
end MIPS;

architecture Behavioral of MIPS is
-- program counter	
	signal pc_current      : std_logic_vector(15 downto 0);
	signal pc_next         : std_logic_vector(15 downto 0);
	signal pc_2            : std_logic_vector(15 downto 0);
	signal im_shift_2      : std_logic_vector(15 downto 0);	
	signal jump_shift_1    : std_logic_vector(14 downto 0);
	signal pc_j            : std_logic_vector(15 downto 0);
	signal pc_beq          : std_logic_vector(15 downto 0);
	signal pc_beq2         : std_logic_vector(15 downto 0);
	signal pc_beq2_j       : std_logic_vector(15 downto 0);	
	signal instruction     : std_logic_vector(15 downto 0);
-- program counter control signals	
	signal beq_control     : std_logic;
	signal jr_control      : std_logic;
-- control
	signal reg_dst         : std_logic_vector(1 downto 0);
	signal mem_to_reg      : std_logic_vector(1 downto 0);
	signal alu_op          : std_logic_vector(1 downto 0);
	signal jump            : std_logic;
	signal branch          : std_logic;
	signal mem_read        : std_logic;
	signal mem_write       : std_logic;
	signal alu_src         : std_logic;
	signal reg_write       : std_logic;
-- registers	
	signal reg_write_dest  : std_logic_vector(2 downto 0);
	signal reg_write_data  : std_logic_vector(15 downto 0);
	signal reg_read_addr_1 : std_logic_vector(2 downto 0);
	signal reg_read_data_1 : std_logic_vector(15 downto 0);
	signal reg_read_addr_2 : std_logic_vector(2 downto 0);
	signal reg_read_data_2 : std_logic_vector(15 downto 0);
-- sign extend (4)
	signal im_ext          : std_logic_vector(15 downto 0);
-- mux (5)	
	signal mux_read_data_2 : std_logic_vector(15 downto 0);	
-- alu	
	signal alu_control     : std_logic_vector(2 downto 0);
	signal alu_out         : std_logic_vector(15 downto 0);
	signal zero_flag       : std_logic;
-- data memory	
	signal mem_read_data   : std_logic_vector(15 downto 0);
begin

--------------------------------------------
--               COMPONENTS               --
--------------------------------------------

-- instruction memory 
	instruction_memory : entity work.instruction_memory
		port map (
			pc          => pc_current,
			instruction => instruction
		);

-- control unit 
	control : entity work.control
		port map(
			rst       	     => rst,
			ctl_opcode       => instruction(15 downto 13),
			ctl_reg_dst      => reg_dst,
			ctl_mem_to_reg   => mem_to_reg,
			ctl_alu_op       => alu_op,
			ctl_jump         => jump,
			ctl_branch       => branch,
			ctl_mem_read     => mem_read,
			ctl_mem_write    => mem_write,
			ctl_alu_src      => alu_src,
			ctl_reg_write    => reg_write			
		);

-- register file
	register_file : entity work.registers
		port map	(
			clk             => clk,
			rst             => rst,
			reg_write       => reg_write,
			reg_write_dest  => reg_write_dest,
			reg_write_data  => reg_write_data,
			reg_read_addr_1 => reg_read_addr_1,
			reg_read_data_1 => reg_read_data_1,
			reg_read_addr_2 => reg_read_addr_2,
			reg_read_data_2 => reg_read_data_2
		);

-- ALU 
	alu : entity work.alu 
		port map(
			alu_in_1    => reg_read_data_1,
			alu_in_2    => mux_read_data_2,
			alu_control => alu_control,
			alu_result  => alu_out,
			alu_zero    => zero_flag
		);	
	
-- ALU control 
	alu_control_uut : entity work.alu_control_comp 	
		port map (
			actl_aluop       	 => alu_op,
			actl_alu_instruction => instruction(2 downto 0),
			actl_alu_control     => alu_control
		);

-- data memory
	data_memory : entity work.data_memory 
		port map	(
			clk                => clk,
			dm_mem_access_addr => alu_out,
			dm_mem_write_data  => reg_read_data_2,
			dm_mem_write_en    => mem_write,
			dm_mem_read        => mem_read,
			dm_mem_read_data   => mem_read_data
		);


--------------------------------------------
--                PROCESS                 --
--------------------------------------------


-- Program counter
	process (clk, rst)
	begin
		if (rst = '1') then
			pc_current <= x"0000";
		elsif (rising_edge(clk)) then
			pc_current <= pc_next;
		end if;
	end process;
	
-- Program counter instructions

-- pc + 2 (next instruction) (7) 
	pc_2        <= pc_current + x"0002";

-- pc_j (8)
	pc_j        <= pc_2(15) & jump_shift_1;

-- sign extend  (9)
	im_shift_2  <= im_ext(14 downto 0) & '0';	

-- pc beq add (10)	
	pc_beq      <= pc_2 + im_shift_2;

-- beq control (11)
	beq_control <= branch and zero_flag;

-- pc_beq (12)
	pc_beq2     <= pc_beq when beq_control = '1' 
					else pc_2;
					
-- pc_beq2_j (13)
	pc_beq2_j    <= pc_j when jump = '1' 
					else pc_beq2;

-- multiplexer jump register control (-)
	jr_control   <= '1' when ((alu_op = "00") and (instruction(3 downto 0) = "1000")) 
					else '0';

-- pc_next and jump register control
	-- reg_read_data_1 is pc_jr
	pc_next     <= reg_read_data_1 when (jr_control = '1') 
					else pc_beq2_j;



-- jump shift left 1 (1)
	jump_shift_1 <= instruction(13 downto 0) & '0';


-- multiplexer regdst (2)
	reg_write_dest <= "111" when reg_dst = "10" 
							else instruction(6 downto 4) when reg_dst = "01" 
							else instruction(9 downto 7);
	
-- register file (3)
	reg_read_addr_1 <= instruction(12 downto 10);
	reg_read_addr_2 <= instruction(9 downto 7);
	
-- sign extend (4) (verificar si se extiende el signo)
	im_ext <= "000000000" & instruction(6 downto 0) when instruction(6) = '0' 
			else "111111111" & instruction(6 downto 0);
			  

-- multiplexer alu_src	(5)	
	mux_read_data_2 <= im_ext when alu_src = '1' 
						else reg_read_data_2;
		
-- data memory (6)
	-- write back 
	reg_write_data <= pc_2 when (mem_to_reg = "10") 
						else mem_read_data when (mem_to_reg = "01") 
						else alu_out;
	
-- output
	pc_out     <= pc_current;
	alu_result <= alu_out;

end Behavioral;
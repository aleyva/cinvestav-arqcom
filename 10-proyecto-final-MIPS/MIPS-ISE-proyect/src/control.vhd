library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- VHDL code for Control Unit of the MIPS Processor
entity control is
	port
	(
		rst        	     : in std_logic;	
		ctl_opcode       : in std_logic_vector(2 downto 0);		
		ctl_reg_dst      : out std_logic_vector(1 downto 0);
		ctl_mem_to_reg   : out std_logic_vector(1 downto 0);
		ctl_alu_op       : out std_logic_vector(1 downto 0);
		ctl_jump         : out std_logic;
		ctl_branch       : out std_logic;
		ctl_mem_read     : out std_logic;
		ctl_mem_write    : out std_logic;
		ctl_alu_src      : out std_logic;
		ctl_reg_write    : out std_logic		
	);
end control;

architecture Behavioral of control is

begin
	process (rst, ctl_opcode)
	begin
		if (rst = '1') then
			ctl_reg_dst      <= "00";
			ctl_mem_to_reg   <= "00";
			ctl_alu_op       <= "00";
			ctl_jump         <= '0';
			ctl_branch       <= '0';
			ctl_mem_read     <= '0';
			ctl_mem_write    <= '0';
			ctl_alu_src      <= '0';
			ctl_reg_write    <= '0';			
		else
			case ctl_opcode is
				when "000" => -- add
					ctl_reg_dst      <= "01";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "00";
					ctl_jump         <= '0';
					ctl_branch       <= '0';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '0';
					ctl_reg_write    <= '1';									
				when "010" => -- j
					ctl_reg_dst      <= "00";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "00";
					ctl_jump         <= '1';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '0';
					ctl_reg_write    <= '0';									
				when "100" => -- lw
					ctl_reg_dst      <= "00";
					ctl_mem_to_reg   <= "01";
					ctl_alu_op       <= "11";
					ctl_jump         <= '0';
					ctl_branch       <= '0';
					ctl_mem_read     <= '1';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '1';
					ctl_reg_write    <= '1';					
				when "101" => -- sw
					ctl_reg_dst      <= "00";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "11";
					ctl_jump         <= '0';
					ctl_branch       <= '0';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '1';
					ctl_alu_src      <= '1';
					ctl_reg_write    <= '0';					
				when "110" => -- beq
					ctl_reg_dst      <= "00";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "01";
					ctl_jump         <= '0';
					ctl_branch       <= '1';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '0';
					ctl_reg_write    <= '0';					
				when "111" => -- addi
					ctl_reg_dst      <= "00";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "11";
					ctl_jump         <= '0';
					ctl_branch       <= '0';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '1';
					ctl_reg_write    <= '1';					
				when others =>
					ctl_reg_dst      <= "01";
					ctl_mem_to_reg   <= "00";
					ctl_alu_op       <= "00";
					ctl_jump         <= '0';
					ctl_branch       <= '0';
					ctl_mem_read     <= '0';
					ctl_mem_write    <= '0';
					ctl_alu_src      <= '0';
					ctl_reg_write    <= '1';					
			end case;
		end if;
	end process;

end Behavioral;
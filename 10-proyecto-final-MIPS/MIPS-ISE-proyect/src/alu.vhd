library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_signed.all;
use IEEE.NUMERIC_STD.all;

entity alu is
	port
	(
		alu_in_1    : in std_logic_vector(15 downto 0);  
		alu_in_2    : in std_logic_vector(15 downto 0);  
		alu_control : in std_logic_vector(2 downto 0);   
		alu_result  : out std_logic_vector(15 downto 0); 
		alu_zero    : out std_logic                      
	);
end alu;

architecture Behavioral of alu is
	signal result : std_logic_vector(15 downto 0);
begin
	process (alu_control, alu_in_1, alu_in_2)
	begin
		case alu_control is
			when "000" => -- add
				result <= alu_in_1 + alu_in_2; 
			when "001" => -- sub
				result <= alu_in_1 - alu_in_2; 
			when "010" => -- and
				result <= alu_in_1 and alu_in_2; 
			when "011" => -- or
				result <= alu_in_1 or alu_in_2; 						
			when "100" => -- slt (set less than)
				if (alu_in_1 < alu_in_2) then	
					result <= x"0001";
				else
					result <= x"0000";
				end if;
			when "101" => -- xor
				result <= alu_in_1 xor alu_in_2; 
			when "110" => -- nor
				result <= alu_in_1 nor alu_in_2; 
			when "111" => -- mul										
				result <= std_logic_vector(to_unsigned((to_integer(unsigned(alu_in_1)) 
								* to_integer(unsigned(alu_in_2))),16)) ;	
			when others => -- add
				result <= alu_in_1 + alu_in_2; 
		end case;
	end process;
	alu_zero   <= '1' when result = x"0000" else '0';
	alu_result <= result;
end Behavioral;
/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/spidey/Git/others/MIPS-processor2/MIPS/src/instruction_memory.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_2592010699;
extern char *IEEE_P_1242562249;

int ieee_p_1242562249_sub_17802405650254020620_1035706684(char *, char *, char *);


static void work_a_3219856015_3212880686_p_0(char *t0)
{
    char t5[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    unsigned char t11;
    int t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    char *t18;
    char *t19;
    int t20;
    int t21;
    int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;

LAB0:    t1 = (t0 + 3336U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 2376U);
    t3 = (t0 + 7673);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 16;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (16 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(52, ng0);

LAB4:    t2 = (t0 + 2376U);
    t10 = std_textio_endfile(t2);
    t11 = (!(t10));
    if (t11 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2376U);
    std_textio_file_close(t2);
    xsi_set_current_line(73, ng0);

LAB18:    *((char **)t1) = &&LAB19;

LAB1:    return;
LAB5:    xsi_set_current_line(53, ng0);
    t3 = (t0 + 3144);
    t4 = (t0 + 2376U);
    t6 = (t0 + 2552U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(54, ng0);
    t2 = (t0 + 3144);
    t3 = (t0 + 2552U);
    t4 = (t0 + 2760U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t0 + 7272U);
    std_textio_read14(STD_TEXTIO, t2, t3, t7, t6);
    xsi_set_current_line(56, ng0);
    t2 = (t0 + 7689);
    *((int *)t2) = 1;
    t3 = (t0 + 7693);
    *((int *)t3) = 16;
    t8 = 1;
    t12 = 16;

LAB8:    if (t8 <= t12)
        goto LAB9;

LAB11:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t8 = *((int *)t3);
    t12 = (t8 + 1);
    t2 = (t0 + 1808U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((int *)t2) = t12;
    goto LAB4;

LAB6:;
LAB9:    xsi_set_current_line(57, ng0);
    t4 = (t0 + 2760U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t0 + 7689);
    t13 = *((int *)t6);
    t14 = (t13 - 1);
    t9 = (t14 * 1);
    xsi_vhdl_check_range_of_index(1, 16, 1, *((int *)t6));
    t15 = (1U * t9);
    t16 = (0 + t15);
    t17 = (t7 + t16);
    t10 = *((unsigned char *)t17);
    t18 = (t0 + 2048U);
    t19 = *((char **)t18);
    t18 = (t19 + 0);
    *((unsigned char *)t18) = t10;
    xsi_set_current_line(58, ng0);
    t2 = (t0 + 2048U);
    t3 = *((char **)t2);
    t10 = *((unsigned char *)t3);
    t11 = (t10 == (unsigned char)48);
    if (t11 != 0)
        goto LAB12;

LAB14:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t13 = *((int *)t3);
    t14 = (t13 - 0);
    t9 = (t14 * 1);
    t15 = (16U * t9);
    t16 = (0U + t15);
    t2 = (t0 + 7689);
    t20 = *((int *)t2);
    t21 = (16 - t20);
    t22 = (t21 - 15);
    t23 = (t22 * -1);
    t24 = (1 * t23);
    t25 = (t16 + t24);
    t4 = (t0 + 4248);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t17 = (t7 + 56U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)3;
    xsi_driver_first_trans_delta(t4, t25, 1, 0LL);

LAB13:
LAB10:    t2 = (t0 + 7689);
    t8 = *((int *)t2);
    t3 = (t0 + 7693);
    t12 = *((int *)t3);
    if (t8 == t12)
        goto LAB11;

LAB15:    t13 = (t8 + 1);
    t8 = t13;
    t4 = (t0 + 7689);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    xsi_set_current_line(59, ng0);
    t2 = (t0 + 1808U);
    t4 = *((char **)t2);
    t13 = *((int *)t4);
    t14 = (t13 - 0);
    t9 = (t14 * 1);
    t15 = (16U * t9);
    t16 = (0U + t15);
    t2 = (t0 + 7689);
    t20 = *((int *)t2);
    t21 = (16 - t20);
    t22 = (t21 - 15);
    t23 = (t22 * -1);
    t24 = (1 * t23);
    t25 = (t16 + t24);
    t6 = (t0 + 4248);
    t7 = (t6 + 56U);
    t17 = *((char **)t7);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    *((unsigned char *)t19) = (unsigned char)2;
    xsi_driver_first_trans_delta(t6, t25, 1, 0LL);
    goto LAB13;

LAB16:    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}

static void work_a_3219856015_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned int t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(76, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = (15 - 4);
    t4 = (t3 * 1U);
    t5 = (0 + t4);
    t1 = (t2 + t5);
    t6 = (t0 + 4312);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 4U);
    xsi_driver_first_trans_fast(t6);

LAB2:    t11 = (t0 + 4152);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3219856015_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t4;
    unsigned char t5;
    char *t6;
    char *t7;
    char *t8;
    int t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 7697);
    t4 = ((IEEE_P_2592010699) + 4000);
    t5 = xsi_vhdl_lessthan(t4, t2, 16U, t1, 16U);
    if (t5 != 0)
        goto LAB3;

LAB4:
LAB5:    t20 = (t0 + 7713);
    t22 = (t0 + 4376);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t20, 16U);
    xsi_driver_first_trans_fast_port(t22);

LAB2:    t27 = (t0 + 4168);
    *((int *)t27) = 1;

LAB1:    return;
LAB3:    t6 = (t0 + 1512U);
    t7 = *((char **)t6);
    t6 = (t0 + 1352U);
    t8 = *((char **)t6);
    t6 = (t0 + 7224U);
    t9 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t8, t6);
    t10 = (t9 - 0);
    t11 = (t10 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t9);
    t12 = (16U * t11);
    t13 = (0 + t12);
    t14 = (t7 + t13);
    t15 = (t0 + 4376);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t14, 16U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB2;

LAB6:    goto LAB2;

}


extern void work_a_3219856015_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3219856015_3212880686_p_0,(void *)work_a_3219856015_3212880686_p_1,(void *)work_a_3219856015_3212880686_p_2};
	xsi_register_didat("work_a_3219856015_3212880686", "isim/tb_MIPS_isim_beh.exe.sim/work/a_3219856015_3212880686.didat");
	xsi_register_executes(pe);
}

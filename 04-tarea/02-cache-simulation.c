

/*=============================================================================
 |   Assignment:  Arquitectura de computadoras 16/03/2018
 |
 |       Author:  Antonio Leyva 
 |   To Compile:  gcc cache-simulation.c -o cache-simulation
 |
 |        Class:  Arquitectura de computadoras  
 |     Due Date:  16/03/2018
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Cache L1 and main memory simulation |
 |        Input:  
 |       Output:  
 |    Algorithm:  
 |   Required Features Not Included:  
 |
 |   Known Bugs:  None
 |
 *===========================================================================*/

//direct mapping
//4 registros
//16 direcciones en L1 cache
// main memory


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define REG_SIZE 4
#define L1_SIZE 16

struct m_register
{
    int vf;     // Bit verification
    int tag;    // Tag, 2 bits for mapping 4 locations    
                // Offset 
    int data;   // Data, 8 bits
};

struct l1cache_register{    
    int data; //Data, 8 bits
};

struct m_memory_register{
    int data; //Data, 8 bits
};


int main(int argv, int **argc){
    // declare m_register and l1cache_register
    struct m_register registers[REG_SIZE];
    struct l1cache_register l1cache[L1_SIZE];
    struct l1cache_register l1cache_block;
    struct m_memory_register main_memory;    
    int i;  //for count in loops 
    int r_or_w; //read or write
    int data; // the data
    int times;   
    int request; //the address of request in l1cache
    int reg_request; //the register request
    int miss;   //miss count
    int hit;    //hit count
    int write_count; //count for write data
    srand(time(NULL));   // Seed initialization
    

    // data initialize in registers
    for(i = 0; i < REG_SIZE; i++){
        registers[i].vf = 0;
        registers[i].tag = 0;
        registers[i].data = rand();    
    }    

    // data initialize in l1cache
    for(i = 0; i < L1_SIZE; i++){        
        l1cache[i].data = rand();    
    }
    
    //initialize hit and miss
    miss = 0;
    hit = 0;
    write_count = 0;
    times = 1000;


    //simulation
    for(i = 0; i < times; i++){
        //¿read or write?
        r_or_w = rand()%2;

        if (r_or_w == 0){   //read simulation
            //program request data randomly
            request = rand()%L1_SIZE;        

            //verify if exist in registers
            reg_request = request % REG_SIZE;
            if(registers[reg_request].vf == 1 && registers[reg_request].tag == request)
                //read from registers
                hit++;
            else
            {
                //read from l1cache, write in register
                l1cache_block = l1cache[request];  
                //write in register 
                registers[reg_request].vf = 1;
                registers[reg_request].tag = request;
                registers[reg_request].data = l1cache_block.data;
                miss++;
            }     

        }else{          //write simulation part
            //the main memory is not mapping, just for send data

            //program determine where to write data randomly
            request = rand()%L1_SIZE;            

            //move the old value to main memory
            main_memory.data = l1cache[request].data;

            //get the new data value
            data = rand();   //write random data
            //now write the new value in register
            reg_request = request % REG_SIZE;            
            registers[reg_request].vf = 1;
            registers[reg_request].tag = request;
            registers[reg_request].data = data;           
            //write the new value in l1cache
            l1cache[request].data = data;
            
            write_count++;
        }  
    }
    //results
    printf("In %d times reading, there were %d hits, %d misses\n", times-write_count, hit, miss);
    printf("Were %d times writing\n", write_count);
    printf("\n");
    
    return 0;
}
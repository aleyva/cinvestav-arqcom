/*
se compila con :
g++ -march=native -finline-functions -O3 -o a.out main.cpp

ejecuta con:
./a.out
*/

#include "header.h"
#include "read_images.cpp"
#include "evaluation.cpp"

    typedef uint64_t UINT64;
    UINT64 start_clk,end_clk;
    double total;


__inline UINT64 get_Clks(void) {
    UINT64 tmp;
    __asm__ volatile(
            "rdtsc\n\t\
            mov %%eax,(%0)\n\t\
            mov %%edx,4(%0)"::"rm"(&tmp):"eax","edx");
    return tmp;
}

#define MEASURE(x)  for (i=0; i< WARMUP; i++)           \
                                 {x;}                   \
                    start_clk=get_Clks();               \
                    for (i = 0; i < REPEAT; i++)        \
                    {                                   \
                                 {x;}                   \
                    }                                   \
                    end_clk=get_Clks();                 \
                    total=(double)(end_clk-start_clk)/REPEAT;


int main() {
		
    double* pesos = new double[LETRAS_DIFERENTES*VECTOR_SIZE];
    int *exitos = new int[5];
    int *exitos_i = new int[5];
    int i;

    // leer_pesos("C:\\Users\\Sergio\\Documents\\ExamenArqui\\W.txt", pesos);
    // read_images("C:\\Users\\Sergio\\Documents\\ExamenArqui\\dataset");
    leer_pesos("W.txt", pesos);
    read_images("dataset");
    normalize();	

    //---------------------------------------------------------------------
	
	MEASURE(naive(pesos, exitos)); //Primera ejecución para tener todo en cache
	MEASURE(naive(pesos, exitos)); //De aqui se toman los tiempos
	cout << endl << "INGENUO" << endl;
    cout << "ciclos: " << total << endl;
    cout << "ciclos por image: " << total/TOTAL_IMAGES << endl << endl;	

    MEASURE(intrinsecs(pesos, exitos_i)); //Primera ejecución para tener todo en cache
	MEASURE(intrinsecs(pesos, exitos_i)); //De aqui se toman los tiempos
	cout << "INTRINSICS" << endl;
    cout << "ciclos: " << total << endl;
    cout << "ciclos por imagen: " << total/TOTAL_IMAGES << endl << endl;	
	    
    //---------------------------------------------------------------------
    
    cout << "Exitos(método INGENUO):\n";
    cout << "A: " << exitos[0] << "/50\n";
    cout << "E: " << exitos[1] << "/50\n";
    cout << "I: " << exitos[2] << "/50\n";
    cout << "O: " << exitos[3] << "/50\n";
    cout << "U: " << exitos[4] << "/50\n\n";

    cout << "Exitos(método INTRINSICS):\n";
    cout << "A: " << exitos_i[0] << "/50\n";
    cout << "E: " << exitos_i[1] << "/50\n";
    cout << "I: " << exitos_i[2] << "/50\n";
    cout << "O: " << exitos_i[3] << "/50\n";
    cout << "U: " << exitos_i[4] << "/50\n\n";

}
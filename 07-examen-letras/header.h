// bibliotecas de main
#include <time.h>
#include <wmmintrin.h> 
#include <emmintrin.h> 
#include <smmintrin.h>
#include <stdint.h>
#include <stdio.h>    

// bibliotecas de read_images
// #include <iostream> // cout, cerr
// #include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>

// bibliotecas de evaluation
#include <immintrin.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

#ifndef REPEAT
    #define REPEAT 10000 //Cuantas veces se va a repetir
#endif
#ifndef WARMUP
    #define WARMUP REPEAT/4
#endif


#define align __attribute__ ((aligned (16)))
#if !defined (ALIGN16) 
# if defined (__GNUC__) 
# define ALIGN16 __attribute__ ( (aligned (16))) 
# else 
# define ALIGN16 __declspec (align (16)) 
# endif 
#endif 

#ifndef BASIC

    #define IMAGE_SIZE 64
    #define IMAGE_SIZE_BIAS 65
    #define TOTAL_IMAGES 250
    #define VECTOR_SIZE 65
    #define LETRAS_DIFERENTES 5    
   
    struct image{
        double data[IMAGE_SIZE_BIAS]; //data of image, including the bias at data[0] and in format 0..1
        unsigned char* raw_data;
        char name;                    //'A' o 'E' ... 'U'
    };
    

    //global variables
    const char LETRAS[5] = {'A','E','I','O','U'};
    struct image images[TOTAL_IMAGES];  //all the images with data

    
    void read_images(const string &directory);
    void normalize();
    void leer_pesos(const char* filename, double* pesos);
    void prod_matriz_vector_i(double* matriz, double* vector, double* res);
    void prod_matriz_vector(double* matriz, double* vector, double* res);
    void naive(double* pesos, int* exitos);
    void intrinsecs(double* pesos, int* exitos);
    
    #define BASIC

#endif /* BASIC */




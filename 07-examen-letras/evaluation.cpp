#include "header.h"


//Recibe la ruta en la que está W.txt y el espacio en el que se almacenarán los pesos.
//pesos debe de tener el espacio previamente asignado.
void leer_pesos(const char* filename, double* pesos){

    ifstream myReadFile;
     myReadFile.open(filename);

    char output[100];
    double output_d;

    if (myReadFile.is_open()) {
        for(int i = 0; i < VECTOR_SIZE; i++)
            for(int j = 0; j < LETRAS_DIFERENTES; j++){
                myReadFile >> output;
                output_d = atof(output);

                pesos[i+(j*VECTOR_SIZE)] = output_d;
            }
    }

    myReadFile.close();
}

//Función de multiplicación CON Intrinsecs. Se asume que el tamaño de los operandos
//  es el definido por las constantes.
void prod_matriz_vector_i(double* matriz, double* vector, double* res){

    __m256d a, b, c_acum;

    //Ejecutamos el producto del vector por cada línea de la matriz.
    for(int i = 0; i < LETRAS_DIFERENTES; i++){

        double aux[4];
        c_acum = _mm256_setzero_pd();


        //Ejecutamos las multiplicaciones en grupos de 4 dobles.
        int j = 0;
        for(j = 0; j < VECTOR_SIZE - 4; j += 4){
            a = _mm256_loadu_pd(vector + j); //Vector
            b = _mm256_loadu_pd(matriz + i*65 + j); //Matriz
            c_acum = _mm256_add_pd(c_acum, _mm256_mul_pd(b,a));
        }

        //Guardamos la suma de las multiplicaciones calculadas.
        _mm256_storeu_pd(aux, c_acum);

        //Sumamos los resultados parciales en la posición correspondiente del vector.
        res[i] = 0;
        for(int j = 0; j < 4; j++)
            res[i] += aux[j];

        //multiplicamos los elementos que sobraron (que son a lo más 3).
        for(; j < VECTOR_SIZE; j++)
            res[i] += matriz[i*VECTOR_SIZE+j]*vector[j];

    }
}


//Función de multiplicación SIN Intrinsecs. Se asume que el tamaño de los operandos
//  es el definido por las constantes.
void prod_matriz_vector(double* matriz, double* vector, double* res){
    for(int i = 0; i < LETRAS_DIFERENTES; i++){
        res[i] = 0;
        for(int j = 0; j < VECTOR_SIZE; j++){
            res[i] = res[i] + (matriz[i*VECTOR_SIZE + j]*vector[j]);
        }
    }
}

char elegir_letra(double* resultados){

    int mayor = 0;

    for(int i = 1; i < LETRAS_DIFERENTES; i++)
        if(resultados[i] > resultados[mayor])
            mayor = i;

    return LETRAS[mayor];
}


//*******************************************************************************************************
//**************************************FUNCIONES FINALES************************************************
//*******************************************************************************************************

void naive(double* pesos, int* exitos){

    double* resultado = new double[LETRAS_DIFERENTES];
    char res;

    for(int i = 0; i < LETRAS_DIFERENTES; i++)
        exitos[i] = 0;

    //Repetimos para cada imagen
    for (int i = 0; i < TOTAL_IMAGES; i++){

        //Evaluamos la imagen
        prod_matriz_vector(pesos, images[i].data, resultado);

        res = elegir_letra(resultado);

        if(images[i].name == res){
            exitos[0] += res=='A'?1:0;
            exitos[1] += res=='E'?1:0;
            exitos[2] += res=='I'?1:0;
            exitos[3] += res=='O'?1:0;
            exitos[4] += res=='U'?1:0;
        }

    }
}

void intrinsecs(double* pesos, int* exitos){

    double* resultado = new double[LETRAS_DIFERENTES];
    char res;

    for(int i = 0; i < LETRAS_DIFERENTES; i++)
        exitos[i] = 0;

    //Repetimos para cada imagen
    for (int i = 0; i < TOTAL_IMAGES; i++){

        //Evaluamos la imagen
        prod_matriz_vector_i(pesos, images[i].data, resultado);

        res = elegir_letra(resultado);

        if(images[i].name == res){
            exitos[0] += res=='A'?1:0;
            exitos[1] += res=='E'?1:0;
            exitos[2] += res=='I'?1:0;
            exitos[3] += res=='O'?1:0;
            exitos[4] += res=='U'?1:0;
        }

    }
}


#include "header.h"

void read_images(const string &directory){
    char line[100];
    int num_image;
    int i=0;
    FILE *fd;

    DIR *dir;
    class dirent *ent;
    class stat st;

    //read each file in "directory"
    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const string file_name = ent->d_name;
        const string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
            continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

        // cout << i <<" : " << file_name.c_str()<< endl;

        //read all the files one by one
        fd = fopen(full_file_name.c_str(), "rb");
        if (fd == NULL) {
            cout << "Could not open image: " << file_name << endl;
            exit(1);
        }

        // skip first line and assume is in the right format
        fgets(line, sizeof(line), fd);

        // read image data
        images[i].raw_data = (unsigned char*)malloc(IMAGE_SIZE*sizeof(char));
        if (fread(images[i].raw_data, sizeof(unsigned char), IMAGE_SIZE, fd) != IMAGE_SIZE) {
            cout << "could not read image: " << file_name << endl;
            fclose(fd);
            exit(1);
        }
        fclose(fd);

        //get the letter name from file
        images[i].name = file_name[0];

        i++;
    }
    // release memory
    closedir(dir);

}

//Transpose raw_data and then Convert the raw_data image to data applyng a division of 255
void normalize(){
    int i, j;

    //Trasponer raw_data
    for(int ii = 0; ii < TOTAL_IMAGES; ii++){
        for(int jj = 0; jj < 8; jj++){
            for(int kk = jj; kk < 8; kk++){
                unsigned char aux = images[ii].raw_data[jj*8 + kk];
                images[ii].raw_data[jj*8 + kk] = images[ii].raw_data[jj + kk*8];
                images[ii].raw_data[jj + kk*8] = aux;
            }
        }
    }

    for (i = 0; i < TOTAL_IMAGES; i++){
        images[i].data[0] = 1;
        for (j = 0; j < IMAGE_SIZE; j++){
            images[i].data[j+1] = images[i].raw_data[j] / 255.0;
        }
        // cout << endl;
    }
}





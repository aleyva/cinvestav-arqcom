----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:41:58 01/29/2019 
-- Design Name: 
-- Module Name:    Mux_8_1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux_8a1 is
    Port ( A0 : in  STD_LOGIC_VECTOR (7 downto 0);
			  A1 : in  STD_LOGIC_VECTOR (7 downto 0);
           A2 : in  STD_LOGIC_VECTOR (7 downto 0);
           A3 : in  STD_LOGIC_VECTOR (7 downto 0);
           A4 : in  STD_LOGIC_VECTOR (7 downto 0);
           A5 : in  STD_LOGIC_VECTOR (7 downto 0);
           A6 : in  STD_LOGIC_VECTOR (7 downto 0);
           A7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Salida : out  STD_LOGIC_VECTOR (7 downto 0));
end Mux_8a1;

architecture Behavioral of Mux_8a1 is

begin
   with Sel select
      Salida <= A0 when "000",
                A1 when "001",
                A2 when "010",
					 A3 when "011",
					 A4 when "100",
					 A5 when "101",
					 A6 when "110",
					 A7 when others;
end Behavioral;


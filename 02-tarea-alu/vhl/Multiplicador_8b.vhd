----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:53:31 01/30/2019 
-- Design Name: 
-- Module Name:    Multiplicador_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multiplier_4b is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);           
           Res : out STD_LOGIC_VECTOR (7 downto 0));
end Multiplier_4b;

architecture Behavioral of Multiplier_4b is
component FullAdder is
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           Cin : in  STD_LOGIC;           
           Cout : out  STD_LOGIC;
			  Sal : out  STD_LOGIC);
end component;
signal Carry : STD_LOGIC_VECTOR (10 downto 0);
signal C : STD_LOGIC_VECTOR (14 downto 0);
signal E : STD_LOGIC_VECTOR (5 downto 0);

begin

-- FA0
C(0) <= A(1)and B(0); 
C(1) <= A(0)and B(1);
-- FA1
C(2) <= A(2)and B(0);
C(3) <= A(1)and B(1);
-- FA2
C(4) <= A(3)and B(0);
C(5) <= A(2)and B(1);
-- FA3
C(6) <= A(3)and B(1);
-- FA4
C(7) <= A(0)and B(2);
-- FA5
C(8) <= A(1)and B(2);
-- FA6
C(9) <= A(2)and B(2);
-- FA7
C(10) <= A(3)and B(2);
-- FA8
C(11) <= A(0)and B(3);
-- FA9
C(12) <= A(1)and B(3);
-- FA10
C(13) <= A(2)and B(3);
-- FA11
C(14) <= A(3)and B(3);

-- Resultados
Res(0) <= A(0) and B(0); 
--                      A      B        Cin      Cout     Sal
FA0: FullAdder PORT MAP(C(1),  C(0),    '0'     ,Carry(0), Res(1) );
FA1: FullAdder PORT MAP(C(3),  C(2),    Carry(0),Carry(1), E(0) );
FA2: FullAdder PORT MAP(C(5),  C(4),    Carry(1),Carry(2), E(1) );
FA3: FullAdder PORT MAP(C(6),  '0',     Carry(2),Carry(3), E(2) );
FA4: FullAdder PORT MAP(C(7),  E(0),    '0',     Carry(4), Res(2) );
FA5: FullAdder PORT MAP(C(8),  E(1),    Carry(4),Carry(5), E(3) );
FA6: FullAdder PORT MAP(C(9),  E(2),    Carry(5),Carry(6), E(4) );
FA7: FullAdder PORT MAP(C(10), Carry(3),Carry(6),Carry(7), E(5) );
FA8: FullAdder PORT MAP(C(11), E(3),    '0',     Carry(8), Res(3) );
FA9: FullAdder PORT MAP(C(12), E(4),    Carry(8),Carry(9), Res(4) );
FA10: FullAdder PORT MAP(C(13),E(5),    Carry(9),Carry(10),Res(5) );
FA11: FullAdder PORT MAP(C(14),Carry(7),Carry(10),Res(7) , Res(6) );

end Behavioral;


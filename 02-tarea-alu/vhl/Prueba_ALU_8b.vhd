--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:21:15 01/30/2019
-- Design Name:   
-- Module Name:   /mnt/Home/Proyectos/Maestria/07 Arquitectura de computadoras/02 Tareas-Ejercicios/tarea02/Prueba_ALU_8b.vhd
-- Project Name:  ALU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ALU_8b
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Prueba_ALU_8b IS
END Prueba_ALU_8b;
 
ARCHITECTURE behavior OF Prueba_ALU_8b IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU_8b
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);
         Sel : IN  std_logic_vector(2 downto 0);
         Alu_Out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal B : std_logic_vector(7 downto 0) := (others => '0');
   signal Sel : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal Alu_Out : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ALU_8b PORT MAP (
          A => A,
          B => B,
          Sel => Sel,
          Alu_Out => Alu_Out
        );
   
   -- Stimulus process
   stim_proc: process
   begin	
--Sel	AND 000
		Sel <= "000";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	OR  001
		Sel <= "001";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	XOR 010
		Sel <= "010";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	INV 011
		Sel <= "011";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	SUM 100
		Sel <= "100";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	SUB 101
		Sel <= "101";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	MUL 110
		Sel <= "110";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
--Sel	SQR 111
		Sel <= "111";
		A <= "10101010";
		B <= "01010101";		
      wait for period;		
		A <= "00001111";
		B <= "11110000";		
      wait for period;				
		A <= "01110111";
		B <= "01110111";		
      wait for period;
		A <= "11111111";
		B <= "11111111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;
   end process;

END;

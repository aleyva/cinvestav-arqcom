----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:42:24 01/31/2019 
-- Design Name: 
-- Module Name:    Adder_Substractor_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder_Substractor_8b is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Cin : in  STD_LOGIC;
           Sel : in  STD_LOGIC;
           Cout : out  STD_LOGIC;
           Sal : out  STD_LOGIC_VECTOR (7 downto 0));
end Adder_Substractor_8b;

architecture Behavioral of Adder_Substractor_8b is
component Mux_2a1_8b
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC;
           Sal : out  STD_LOGIC_VECTOR (7 downto 0));
end component;
component FullAdder_8b
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Cin : in  STD_LOGIC;
           Cout : out  STD_LOGIC;
			  Sum : out STD_LOGIC_VECTOR (7 downto 0) );           
end component;
signal b_int, b_mux : STD_LOGIC_VECTOR (7 downto 0);
signal b_cout : STD_LOGIC;

begin
FA0: FullAdder_8b port map (not B, "00000001", '0', b_cout, b_mux);
mux2a1_0: Mux_2a1_8b port map (B, b_mux, Sel, b_int);
FA1: FullAdder_8b port map (A, b_int, Cin, Cout, Sal);



end Behavioral;


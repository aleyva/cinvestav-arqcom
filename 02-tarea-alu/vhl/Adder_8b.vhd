----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:15:13 01/29/2019 
-- Design Name: 
-- Module Name:    Adder_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder_8b is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Cin : in  STD_LOGIC;
           Cout : out  STD_LOGIC;
			  Sum : out STD_LOGIC_VECTOR (7 downto 0) );           
end FullAdder_8b;

architecture Behavioral of FullAdder_8b is
component FullAdder
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           Cin : in  STD_LOGIC;           
           Cout : out  STD_LOGIC;
			  Sal : out  STD_LOGIC);
end component;
signal Carry : STD_LOGIC_VECTOR (6 downto 0);
begin
FA0 : FullAdder port map ( A(0),B(0),Cin,     Carry(0),Sum(0) );
FA1 : FullAdder port map ( A(1),B(1),Carry(0),Carry(1),Sum(1) );
FA2 : FullAdder port map ( A(2),B(2),Carry(1),Carry(2),Sum(2) );
FA3 : FullAdder port map ( A(3),B(3),Carry(2),Carry(3),Sum(3) );
FA4 : FullAdder port map ( A(4),B(4),Carry(3),Carry(4),Sum(4) );
FA5 : FullAdder port map ( A(5),B(5),Carry(4),Carry(5),Sum(5) );
FA6 : FullAdder port map ( A(6),B(6),Carry(5),Carry(6),Sum(6) );
FA7 : FullAdder port map ( A(7),B(7),Carry(6),Cout,    Sum(7) );

end Behavioral;


--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:20:09 01/30/2019
-- Design Name:   
-- Module Name:   /mnt/Home/Proyectos/Maestria/07 Arquitectura de computadoras/02 Tareas-Ejercicios/tarea02/Prueba_multiplicador.vhd
-- Project Name:  ALU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Multiplicador_8b
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Prueba_multiplicador IS
END Prueba_multiplicador;
 
ARCHITECTURE behavior OF Prueba_multiplicador IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Multiplicador_8b
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);         
         Res : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal B : std_logic_vector(7 downto 0) := (others => '0');
   signal Cout : std_logic := '0';
   signal Res : std_logic_vector(7 downto 0) := (others => '0');
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Multiplicador_8b PORT MAP (
          A => A,
          B => B,
          Cout => Cout,
          Res => Res
        );


   -- Stimulus process
   stim_proc: process
   begin		      
		A <= "1010";
		B <= "0101";		
      wait for period;		
		A <= "1001";
		B <= "0001";		
      wait for period;				
		A <= "0111";
		B <= "0111";		
      wait for period;
		A <= "1111";
		B <= "1111";		
      wait for period;
		A <= "0000";
		B <= "0000";		      
      wait for period;
		A <= "1111";
		B <= "0000";		
      wait for period;
		A <= "0000";
		B <= "1111";		      
      wait for period;
			
   end process;

END;

--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:11:08 01/29/2019
-- Design Name:   
-- Module Name:   /mnt/Home/Proyectos/Maestria/07 Arquitectura de computadoras/02 Tareas-Ejercicios/tarea02/PruebaFA_8b.vhd
-- Project Name:  ALU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Adder_8b
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY PruebaFA_8b IS
END PruebaFA_8b;
 
ARCHITECTURE behavior OF PruebaFA_8b IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Adder_8b
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);
         Cin : IN  std_logic;
         Cout : OUT  std_logic;
         Sum : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal B : std_logic_vector(7 downto 0) := (others => '0');
   signal Cin : std_logic := '0';

 	--Outputs
   signal Cout : std_logic;
   signal Sum : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Adder_8b PORT MAP (
          A => A,
          B => B,
          Cin => Cin,
          Cout => Cout,
          Sum => Sum
        );

   

   -- Stimulus process
   stim_proc: process
   begin		
      A <= "10101010";
		B <= "01010101";
		Cin <= '0';
      wait for period;		
		A <= "10011001";
		B <= "00010001";		      
      wait for period;		
		A <= "11111111";
		B <= "11111111";		
      wait for period;				
		A <= "00000000";
		B <= "00000000";		
      wait for period;
   end process;

END;

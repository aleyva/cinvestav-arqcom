--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:45:21 01/30/2019
-- Design Name:   
-- Module Name:   /mnt/Home/Proyectos/Maestria/07 Arquitectura de computadoras/02 Tareas-Ejercicios/tarea02/Prueba_Multiplicador.vhd
-- Project Name:  ALU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Multiplier_4b
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Prueba_Multiplicador IS
END Prueba_Multiplicador;
 
ARCHITECTURE behavior OF Prueba_Multiplicador IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Multiplier_4b
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);
         Res : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal B : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal Res : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Multiplier_4b PORT MAP (
          A => A,
          B => B,
          Res => Res
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
		A <= "00001010";
		B <= "00000101";		
      wait for period;		
		A <= "00001001";
		B <= "00000001";		
      wait for period;				
		A <= "00000111";
		B <= "00000111";		
      wait for period;
		A <= "00001111";
		B <= "00001111";		
      wait for period;
		A <= "00000000";
		B <= "00000000";		      
      wait for period;		
		A <= "00001111";
		B <= "00000000";		
      wait for period;
		A <= "00000000";
		B <= "00001111";		      
      wait for period;
   end process;

END;

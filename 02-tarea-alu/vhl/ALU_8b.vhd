----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:27:59 01/29/2019 
-- Design Name: 
-- Module Name:    ALU_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU_8b is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Alu_Out : out  STD_LOGIC_VECTOR (7 downto 0));
end ALU_8b;

architecture Behavioral of ALU_8b is
component Adder_Substractor_8b
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Cin : in  STD_LOGIC;
           Sel : in  STD_LOGIC;
           Cout : out  STD_LOGIC;
           Sal : out  STD_LOGIC_VECTOR (7 downto 0));
end component;
component Multiplier_4b
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);           
           Res : out STD_LOGIC_VECTOR (7 downto 0));
end component;
component Mux_8a1
    Port ( A0 : in  STD_LOGIC_VECTOR (7 downto 0);
			  A1 : in  STD_LOGIC_VECTOR (7 downto 0);
           A2 : in  STD_LOGIC_VECTOR (7 downto 0);
           A3 : in  STD_LOGIC_VECTOR (7 downto 0);
           A4 : in  STD_LOGIC_VECTOR (7 downto 0);
           A5 : in  STD_LOGIC_VECTOR (7 downto 0);
           A6 : in  STD_LOGIC_VECTOR (7 downto 0);
           A7 : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC_VECTOR (2 downto 0);
           Salida : out  STD_LOGIC_VECTOR (7 downto 0));
end component;
signal Alu_And : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Or : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Xor : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Inv : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Sum : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Res : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Mul : STD_LOGIC_VECTOR (7 downto 0);
signal Alu_Sqr : STD_LOGIC_VECTOR (7 downto 0);
signal Bandera_AndSub : STD_LOGIC;

begin

-- operaciones
Alu_And <= A AND B;
Alu_Or <= A OR B;
Alu_Xor <= A XOR B;
Alu_Inv <= NOT A;
add_sub : Adder_Substractor_8b port map (A,B,'0',Sel(0),Bandera_AndSub,Alu_Sum);
--temp2 : Adder_Substractor_8b port map (A,B,Sel(0),Bandera_Res,Alu_Res);
Alu_Res <= Alu_Sum;
mult : Multiplier_4b port map (A,B,Alu_Mul);
sqre : Multiplier_4b port map (A,A,Alu_Sqr);

-- mux
mux : Mux_8a1 port map (Alu_And,Alu_Or,Alu_Xor,Alu_Inv,Alu_Sum,Alu_Res,Alu_Mul,Alu_Sqr,Sel,Alu_Out);

end Behavioral;


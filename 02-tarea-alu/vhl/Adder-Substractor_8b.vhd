----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:46:36 01/29/2019 
-- Design Name: 
-- Module Name:    Adder-Substractor_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder_Substractor_8b is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           Sel : in  STD_LOGIC;
           Cout : out  STD_LOGIC;
           Res : out  STD_LOGIC_VECTOR (7 downto 0));
end Adder_Substractor_8b;

architecture Behavioral of Adder_Substractor_8b is
component FullAdder
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           Cin : in  STD_LOGIC;           
           Cout : out  STD_LOGIC;
			  Sal : out  STD_LOGIC);
end component;
signal Carry : STD_LOGIC_VECTOR (6 downto 0);
signal Baux : STD_LOGIC_VECTOR (7 downto 0);


begin

--Baux <= B XOR Sel;
Baux(0) <= B(0) XOR Sel;
Baux(1) <= B(1) XOR Sel;
Baux(2) <= B(2) XOR Sel;
Baux(3) <= B(3) XOR Sel;
Baux(4) <= B(4) XOR Sel;
Baux(5) <= B(5) XOR Sel;
Baux(6) <= B(6) XOR Sel;
Baux(7) <= B(7) XOR Sel;
AS0 : FullAdder port map (A(0),Baux(0),Sel,     Carry(0),Res(0));
AS1 : FullAdder port map (A(1),Baux(1),Carry(0),Carry(1),Res(1));
AS2 : FullAdder port map (A(2),Baux(2),Carry(1),Carry(2),Res(2));
AS3 : FullAdder port map (A(3),Baux(3),Carry(2),Carry(3),Res(3));
AS4 : FullAdder port map (A(4),Baux(4),Carry(3),Carry(4),Res(4));
AS5 : FullAdder port map (A(5),Baux(5),Carry(4),Carry(5),Res(5));
AS6 : FullAdder port map (A(6),Baux(6),Carry(5),Carry(6),Res(6));
AS7 : FullAdder port map (A(7),Baux(7),Carry(6),Cout,Res(7));


end Behavioral;


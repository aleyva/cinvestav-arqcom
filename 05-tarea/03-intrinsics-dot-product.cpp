
// Compile instrucction
// g++ --std=c++14 -O2 -mavx 05-intrinsics-dot-product.cpp -o 05-intrinsics-dot-product

//execute instruction
// ./05-intrinsics-dot-product 


#include <stdio.h>

#ifdef __AVX__
  #include <immintrin.h>
#else
  #warning AVX is not available. Code will not compile!
#endif

#define M 50
#define N 512
    

int main (int argc, char *argv[]) { 
    float result[N], temp[N], b[M][N], c[N];      
    int i, j;  
    
    printf("Multiply Matrix %dx%d by Vector %d\n",M,N,N);

    //populate the matrix c
    for (j=0; j<N; j++) {
        c[j] = 1.0;
        temp[j] = 0;
        result[j] = 0;        
    }

    //populate the matrix b
    for (int i=0; i<M; i++)
        for (j=0; j<N; j++) 
            b[i][j] = 1.0;                
    
    
    //Do the dot product
    if(1){  //verify if 'b' can multiply with 'c'
        __m256 vec_b, vec_c, vec_temp, res;
        for(i=0; i<M; i++){ 
            
            //multiply each row with the c matrix with avx
            for (j=0; j < N ; j+=8){                
                //the multiplication part with avx
                vec_b = _mm256_loadu_ps(&b[i][j]);
                vec_c = _mm256_loadu_ps(&c[j]);
                res  = _mm256_mul_ps(vec_b,vec_c);
                
                //the addition part with avx
                if(j!=0){
                    vec_temp = _mm256_loadu_ps(&temp[0]);
                    res  = _mm256_add_ps(vec_temp,res);                    
                }
                _mm256_storeu_ps(&temp[0],res);
            }           
        
            //finally add secuentially the first 8 positions of the array
            for(j=0; j<8; j++){
                result[i] += temp[j];
            }
        }
    } 
    
    // showing the result
    printf("\nResult: \n");           
    for (j=0; j<M; j++) {
        printf("%3.1f ", result[j]);
    }    
    printf("\n");
    return 0;
} 
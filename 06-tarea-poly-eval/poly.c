
/*

poly.h define la interfaz que deben usar, esto es que la función que hagan debe ser definida como:
double poly_eval(double X,double *A, long long sizea)
    X es la variable sobre la cuál se evaluará el polinomio.
    A es un arreglo donde se almacenan los coeficientes.
    sizea es el número de coeficientes.

El archivo timing_x86.c mide el número de ciclos de reloj que utiliza su
función y cuántos ciclos usará para procesar cada coeficiente.

compilen con:

gcc -march=native -finline-functions -O3 -o a.out poly.c timing_x86.c

Y ejecuten a.out

Para ver el correcto funcionamiento hagan un main en poly.c y chequen con
la implementación naive, cuando midan la velocidad comenten dicho main.
*/


#include "poly.h"
#include <math.h>
#ifdef __AVX__
  #include <immintrin.h>
#else
  #warning AVX is not available. Code will not compile!
#endif

//double horner(double X,double *A, long long sizea){    
/*
double poly_eval(double X,double *A, long long sizea){    
    // Initialize result 
    double result = A[0];  
  
    // Evaluate value of polynomial using Horner's method 
    for (int i=1; i<sizea; i++) 
        result = A[i] + result*X; 
  
    return result;
}
*/


//double estrin_intrinsics(double X, double *A, long long sizea){        

double poly_eval(double X, double *A, long long sizea){        
    // Estrin's scheme    
    // P(x) = (Cm + Cm-1x) + (C2 + C3x)x2 + (C4 + C5x)x4 + ⋯ = Q(x2)    
    
    //define and initialize variables
    int i, j, half = sizea/2; 
    // double half_ceil = ceil(half);  //esta variable está pensada para posteriormente evaluar polinomios que no sean multiplo de 4
    double result = 0;
    double x;
    double square_X = X*X;    
    double odd[half];   //impar, no x multiplication
    double even[half];            //par, x multiplication
    double temp[4];
    
    __m256d vec_even, vec_odd, vec_partial_res, vec_res; 
    __m256d vec_x, vec_x_special, vec_pow_x, vec_square;    
    
    temp[0] = temp[1] = temp[2] = temp[3] = X; 
    vec_x = _mm256_loadu_pd(&temp[0]);

    temp[0] = temp[1] = temp[2] = temp[3] = 0; 
    vec_res = _mm256_loadu_pd(&temp[0]);

    //init in 2⁶ because it is start used in the second iteration
    temp[0] = 1;
    temp[1] = square_X;
    temp[2] = temp[1] * square_X;
    temp[3] = temp[2] * square_X;           
    vec_x_special = _mm256_loadu_pd(&temp[0]);       

    //init in 2⁶ because it is start used in the second iteration
    temp[0] = temp[3] * square_X;
    temp[1] = temp[0] * square_X;
    temp[2] = temp[1] * square_X;
    temp[3] = temp[2] * square_X;           
    vec_pow_x = _mm256_loadu_pd(&temp[0]);       

    temp[0] = temp[1] = temp[2] = temp[3] = square_X;
    vec_square = _mm256_loadu_pd(&temp[0]); 

    //divide polynomial in two sets          
    for (i=sizea-1, j=0; i>0; i-=2, j++){                 
        odd[j] = A[i];
        even[j] = A[i-1];        
    }
        
    //multiply with X all the even set and then add with all the odd set
    //calcular si es multiplo de 4    
    for (i=0; i<half; i+=4){   
        vec_even = _mm256_loadu_pd(&even[i]); 
        vec_odd = _mm256_loadu_pd(&odd[i]); 
        vec_partial_res = _mm256_mul_pd(vec_even,vec_x);                
        vec_partial_res = _mm256_add_pd(vec_odd,vec_partial_res);                

        //multiplicar por el x factorizado
        if(i==0){
            vec_partial_res = _mm256_mul_pd(vec_x_special,vec_partial_res);        
        }else{
            vec_partial_res = _mm256_mul_pd(vec_pow_x,vec_partial_res);        
            //se puede mejorar eliminado esta operacion para los 4 ultimos elementos
            vec_pow_x = _mm256_mul_pd(vec_pow_x,vec_square);     
        }                       

        //the addition part with avx
        vec_res  = _mm256_add_pd(vec_res,vec_partial_res);                           
    }

    _mm256_storeu_pd(&temp[0],vec_res);    
    
    return temp[0] + temp[1] + temp[2] + temp[3];
}


/*
double estrin(double X,double *A, long long sizea){
    // P(x) = (Cm + Cm-1x) + (C2 + C3x)x2 + (C4 + C5x)x4 + ⋯ = Q(x2)    
    double x = X*X;        
    // printf("%lf - %lf", A[sizea-1], A[sizea-2]);
    double result = A[sizea-1] + A[sizea-2]*X; 

    for (int i=sizea-3; i>0; i-=2){   
        printf("i: %d, %4.2lf \n", i, x);     
        result += (A[i] + A[i-1]*X) * x;         
        x *= X*X;
    }
    return result;            
}
*/

/*
int main() {
	int size = 1024;
    double A[size];    
	double X;	
	int i;

	srand((unsigned)time(NULL));
	X = (double)(rand()%1000)/800.0f;
	
    printf("p(%4.4lf) = ", X);	    
    for(i=0;i<size;i++){
		A[i] =  (double)(rand()%1000)/800.0f;		
        if(i<size-1)
            printf("%4.4lfx^%d + ", A[i],size-i-1);
        else
            printf("%4.4lf", A[i]);
    }
    
    printf("\n");	
    printf("************************\n");	
	printf("Horner result: %lf\n",horner(X,A,size) );
    printf("************************\n");	
    printf("Strin result: %lf\n",estrin_intrisics(X,A,size) );
    printf("************************\n");	
	return 0;
	
}
*/

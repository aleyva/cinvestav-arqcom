#include "poly.h"
#include <time.h>

#ifndef REPEAT
    #define REPEAT 10000 //Cuantas veces se va a repetir
#endif
#ifndef WARMUP
    #define WARMUP REPEAT/4
#endif

#define	size 16384  // definir el numero de coeficientes en el polinomio


    UINT64 start_clk,end_clk;
    double total;
    
__inline UINT64 get_Clks(void) {
    UINT64 tmp;
    __asm__ volatile(
            "rdtsc\n\t\
            mov %%eax,(%0)\n\t\
            mov %%edx,4(%0)"::"rm"(&tmp):"eax","edx");
    return tmp;
}

#define MEASURE(x)  for (i=0; i< WARMUP; i++)           \
                                 {x;}                   \
                    start_clk=get_Clks();               \
                    for (i = 0; i < REPEAT; i++)        \
                    {                                   \
                                 {x;}                   \
                    }                                   \
                    end_clk=get_Clks();                 \
                    total=(double)(end_clk-start_clk)/REPEAT;


int main() {
	double A[size];
	double X;
	
	int i;

	srand((unsigned)time(NULL));

	X = (double)(rand()%1000)/800.0f;
	
	for(i=0;i<size;i++)
		A[i] =  (double)(rand()%1000)/800.0f;
		
	
	MEASURE(poly_eval(X,A,size);); //Primera ejecución para tener todo en cache
	MEASURE(poly_eval(X,A,size);); //De aqui se toman los tiempos
	printf("%.2f cycles\n\n", total);	
	printf("%.2f cycles per mul\n\n", total/size);	
	
	
}
#include <wmmintrin.h> 
#include <emmintrin.h> 
#include <smmintrin.h>
#include <stdint.h>
#include <stdio.h>


#define align __attribute__ ((aligned (16)))
#if !defined (ALIGN16) 
# if defined (__GNUC__) 
# define ALIGN16 __attribute__ ( (aligned (16))) 
# else 
# define ALIGN16 __declspec (align (16)) 
# endif 
#endif 

typedef uint64_t UINT64;

double poly_eval(double X,double *A, long long sizea);